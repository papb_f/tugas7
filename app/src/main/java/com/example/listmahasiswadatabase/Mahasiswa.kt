package com.example.listmahasiswadatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Mahasiswa")
data class Mahasiswa (
    @ColumnInfo val nim:String,
    @ColumnInfo val name:String
){
    @PrimaryKey(autoGenerate = true)var id:Int?=null
}