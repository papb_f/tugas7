package com.example.listmahasiswadatabase

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
@Database(entities = [Mahasiswa::class], version = 1)
abstract class MahasiswaDatabase : RoomDatabase(){

    abstract fun mahasiswaDao():DAOMahasiswa;

}