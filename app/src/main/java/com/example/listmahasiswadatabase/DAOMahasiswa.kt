package com.example.listmahasiswadatabase

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface DAOMahasiswa {
    @Insert
    fun insertAll(vararg mahasiswa: Mahasiswa);
    @Insert
    fun insert(mahasiswa: Mahasiswa)
    @Delete
    fun delete(mahasiswa: Mahasiswa);
    @Query("SELECT * FROM MAHASISWA")
    fun getAll():List<Mahasiswa>;
}