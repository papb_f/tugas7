package com.example.listmahasiswadatabase

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView

class AdapterMahasiswa(var listMahasiswa: List<Mahasiswa>, context: Context) :
    RecyclerView.Adapter<AdapterMahasiswa.HolderData>() {
    var inflater:LayoutInflater?=null;


    init {
        this.inflater=LayoutInflater.from(context)
    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderData {
        val view:View = inflater!!.inflate(R.layout.item,parent,false)
        return HolderData(view)
    }

    override fun onBindViewHolder( holder: HolderData, position: Int) {
        val item:Mahasiswa = listMahasiswa.get(position)
        holder.textNo.text= item.id.toString()
        holder.textNim.text= item.nim;
        holder.textName.text=item.name;
    }

    override fun getItemCount(): Int {
        return listMahasiswa.size;
    }

    class HolderData(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textNo: TextView = itemView.findViewById(R.id.idView);
        var textNim: TextView = itemView.findViewById(R.id.nimView);
        var textName: TextView = itemView.findViewById(R.id.nameView);


    }
}