package com.example.listmahasiswadatabase

import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val appDb = Room.databaseBuilder(applicationContext,MahasiswaDatabase::class.java,"db").build()
        val rv = findViewById<RecyclerView>(R.id.rv)
        val llm = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv.setLayoutManager(llm)
        var listMahasiswa:List<Mahasiswa> = emptyList()
        val instBut:Button = findViewById(R.id.insert)
        val viewbut:Button = findViewById(R.id.view)
        val nimText:EditText = findViewById(R.id.nimtext)
        val nameText:EditText=findViewById(R.id.nameText)

        instBut.setOnClickListener{
            GlobalScope.launch {
                val mahasiswa= Mahasiswa(nimText.text.toString(),nameText.text.toString());
                appDb.mahasiswaDao().insertAll(mahasiswa)

            }


        }
        viewbut.setOnClickListener{
            GlobalScope.launch {
                listMahasiswa = appDb.mahasiswaDao().getAll()
            }
            val adapter = AdapterMahasiswa(listMahasiswa,this)
            rv.adapter=adapter
            adapter.notifyDataSetChanged()





        }

    }
    companion object{
        private var instance: MainActivity? = null
        public fun getApp():Context{
            return instance!!.applicationContext
        }
    }



}